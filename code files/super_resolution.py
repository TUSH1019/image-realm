import cv2
from tkinter.filedialog import askopenfilename

img_path = askopenfilename()

img = cv2.imread(img_path)
cv2.imshow("og img", img)
print(img.shape)
img = img[:, 200:480]
cv2.imshow("low resolution img", img)

near_img = cv2.resize(img,None, fx = 2, fy = 2, interpolation = cv2.INTER_NEAREST)  
cv2.imshow("nearest neighbours img", near_img)

bilinear_img = cv2.resize(img,None, fx = 2, fy = 2, interpolation = cv2.INTER_LINEAR) #weighted avg of 2 pixels
cv2.imshow("bilinear img", bilinear_img)

bicubic_img = cv2.resize(img,None, fx = 2, fy = 2, interpolation = cv2.INTER_CUBIC)
cv2.imshow("bicubic img", bicubic_img)



cv2.waitKey(0)
cv2.destroyAllWindows()