#Converting an image into a pencil sketch

import cv2 as cv
from tkinter.filedialog import *

filename = askopenfilename() 

img = cv.imread(filename)
cv.imshow("Image", img)

img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
img_invert = cv.bitwise_not(img_gray)
img_smoothing = cv.GaussianBlur(img_invert, (51, 51),sigmaX=0, sigmaY=0)

def blend(x, y):
  return cv.divide(x, 255 - y, scale=256)

final_img = blend(img_gray, img_smoothing)
cv.imshow("pencil sketch", final_img)

cv.waitKey(0)
cv.destroyAllWindows()
