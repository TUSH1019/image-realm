#from img_to_cartoon import cartoonize
from tkinter.filedialog import askopenfilename
import cv2 as cv
import numpy as np

def cartoonize(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.medianBlur(gray, 5)
    edges = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 9, 9)
    color = cv.bilateralFilter(img, 9, 250, 250)
    cartoon = cv.bitwise_and(color, color, mask=edges)
    return cartoon


#out = cv.VideoWriter('out.mp4', cv.VideoWriter_fourcc(*'MP4V'), 24, (720, 1280))
filename = askopenfilename()
cap = cv.VideoCapture(filename) #0 for webcam

while(cap.isOpened()):
    ret, frame = cap.read()

    frame = cartoonize(frame) #, cv2.COLOR_BGR2GRAY)

    cv.imshow('frame',frame)
    #out.write(frame)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv.destroyAllWindows()
