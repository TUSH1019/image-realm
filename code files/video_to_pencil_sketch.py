import cv2 as cv
from tkinter.filedialog import askopenfilename


def blend(x, y):
  return cv.divide(x, 255 - y, scale = 256)

def pencil_sketch(img):
    img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img_invert = cv.bitwise_not(img_gray)
    img_smoothing = cv.GaussianBlur(img_invert, (21, 21),sigmaX=0, sigmaY=0)
    final_img = blend(img_gray, img_smoothing)
    return final_img

filename = askopenfilename()
cap = cv.VideoCapture(filename)

while(cap.isOpened()):
    ret, frame = cap.read()

    frame = pencil_sketch(frame) #, cv2.COLOR_BGR2GRAY)

    cv.imshow('frame',frame)
    #out.write(frame)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv.destroyAllWindows()
